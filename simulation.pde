final float       q          = 1.0e-12;                     // charge of the particle
final float       Q          = 1.0e-12;                     // charge of the sphere
final float       R          = 1.0;                         // radius of the sphere
final FloatVector particleAt = new FloatVector( 3.0, 0.0 ); // location of the particle
final FloatVector sphereAt   = new FloatVector( 0.0, 0.0 ); // location of the sphere

System sys = new System( new Particle( q, particleAt ), new Sphere( R, Q, sphereAt ) );

// graphics parameters:
final float xMax    = 4;        // maximum position to plot
final float vLimit  = 0.02;     // maximum absolute value of the displayed potential
final float eLimit  = 0.05;     // maximum absolute value of the displayed field
final int   nPoints = 1000;     // number of points at which to tabulate functions

float pxPerMeter = 0.0;         // world-to-screen scale; canNOT be initialized here

void setup() {                       // initialize the graphics subsystem
  size( 700, 700 );                  // arguments canNOT be variables, MUST be digits
  pxPerMeter = ( 700 / 2 ) / xMax;   // MUST be initialized here 
  frameRate = 30;
}

void draw() {
  
  background( 255 );
   
  textSize( 20 );
  vText();
  text( "Potential", 100, 70 );
  
  textSize( 20 );
  eText();
  text( "Field", 100, 105 );
  
  // ---------------------------------------------------------- PHYSICAL GEOMETRY ----------------------------------------------------------
  // SPHERE:
  IntVector posOnScreenSphere = toScreen( sys.sphere().position() ); // convert world coordinates of position of sphere in screen coordinates
  
  if ( Q == 0 ) {        // black for uncharged 
    fill( 0, 0, 0 );
  } else if ( Q > 0 ) {  // red for positive charge
    fill( 255, 100, 100 );
  }  
  else {                 // blue for negative charge
    fill( 100, 100, 255 ); //<>//
  }
  
  ellipse( posOnScreenSphere.x(), posOnScreenSphere.y(), 2 * pxPerMeter, 2 * pxPerMeter );
  noFill();
  
  // PARTICLE:
  IntVector posOnScreenParticle = toScreen( sys.particle().position() );
  
  // fill(0, 0, 0);
  
  if ( q == 0 ) {        // black for uncharged 
    fill( 0, 0, 0 );
  } else if ( q > 0 ) {  // red for positive charge
    fill( 255, 100, 100 );
  }  
  else {                 // blue for negative charge
    fill( 100, 100, 255 );
  }
  
  ellipse( posOnScreenParticle.x(), posOnScreenParticle.y(), 20, 20 ); // fat
  noFill();
  
  // ---------------------------------------------------------- ELECTRIC POTENTIAL ----------------------------------------------------------
  
  Voltage[] vs = tabulateVoltage(                                 // tabulate the potential over a range of positions
                    new FloatVector( -xMax, 0.0 ),               // starting position
                    new FloatVector( 2 * xMax / nPoints, 0.0 ),  // position increment
                    nPoints                                      // number of positions to tabulate
                 );
                 
  float vMin = Float.MAX_VALUE; // initialize to value it could never have
  float vMax = Float.MIN_VALUE; // intialize to value it could never have 
  
  for( int i = 0; i < nPoints; i++ ) { // find the minimum and maximum values of the potential
    float v = vs[i].V();
    if ( Math.abs( v ) <= vLimit ) {
      vMin = Math.min( v, vMin );
      vMax = Math.max( v, vMax );
    }
  }
    
  strokeWeight( 4 );  // use thicker lines to plot functions
  vColor();           // set the color for the potential function
  
  for ( int i = 1; i < nPoints; i++ ) {                       // for all the tabulated positions
    int xStart = toScreen( vs[i - 1].r() ).x();               // segment left-endpoint in screen coordinates
    int yStart = voltsToScreen( vs[i - 1].V(), vMin, vMax );
    int xEnd   = toScreen( vs[i].r() ).x();                   // segment right-endpoint in screen coordinates
    int yEnd   = voltsToScreen( vs[i].V(), vMin, vMax );

    line( xStart, yStart, xEnd, yEnd );                       // draw the segment
  }
  
  strokeWeight( 1 );                                          // thin horizontal line to show the zero of the potential scale                 
  int v0 = voltsToScreen(0.0, vMin, vMax);
  line( 0, v0, width, v0 );
  
  // ------------------------------------------------------------- ELECTRIC FIELD ---------------------------------------------------------------
  
  Field[] es = tabulateField(                                    // tabulate the x component of the electric field over the same positions
                    new FloatVector( -xMax, 0.0 ),               // starting position
                    new FloatVector( 2 * xMax / nPoints, 0.0 ),  // position increment
                    nPoints                                      // number of positions to tabulate
               );
                 
  float eMin = Float.MAX_VALUE; // initialize to value it could never have
  float eMax = Float.MIN_VALUE; // intialize to value it could never have 
  
  for ( int i = 0; i < nPoints; i++ ) { // find the minimum and maximum values of the electric field
    float e = es[i].E().x();
    if ( Math.abs( e ) <= eLimit ) {
      eMin = Math.min( e, eMin );
      eMax = Math.max( e, eMax );
    }
  }
  
  strokeWeight( 4 );    // use thicker lines to plot functions
  eColor();             // set the color of the electric-field function
  
  for ( int i = 1; i < nPoints; i++ ) {
    int xStart = toScreen( es[i - 1].r() ).x(); // converts graph intervals to pixels
    int yStart = fieldToScreen( es[i - 1].E().x(), eMin, eMax );
    int xEnd   = toScreen( es[i].r() ).x();
    int yEnd   = fieldToScreen( es[i].E().x(), eMin, eMax );

    line( xStart, yStart, xEnd, yEnd );
  }
  
  strokeWeight( 1 );  // a thin horizontal line to show the zero of the electric field scale
  int e0 = fieldToScreen(0.0, eMin, eMax);
  line( 0, e0, width, e0 );
}

public IntVector toScreen( FloatVector worldCoord ) {                 // converts a world position to screen coordinates 
  return new IntVector(                                               // world (0, 0) corresponds to screen (width/2, height/2)
               new Float( width  / 2 + worldCoord.x() * pxPerMeter ).intValue(),
               new Float( height / 2 - worldCoord.y() * pxPerMeter ).intValue()
             ); 
}

public int voltsToScreen( float v, float vMin, float vMax ) {         // converts a potential value to vertical screen coordinate
  return new Float( height - ( v - vMin ) * height / ( vMax - vMin ) ).intValue();    
}

public int fieldToScreen( float e, float eMin, float eMax ) {         // converts an electric-field value to vertical screen coordinate
  return new Float( height - ( e - eMin ) * height / ( eMax - eMin ) ).intValue();    
}

public Voltage[] tabulateVoltage( FloatVector start, FloatVector delta, int count ) {
  
  FloatVector r  = start;
  Voltage[]   vs = new Voltage[count];
  
  for ( int i = 0; i < count; i++ ) {
    vs[i] = new Voltage( sys.V( r ), r );
    r     = r.add( delta );
  }
  
  return vs;
}

public Field[] tabulateField( FloatVector start, FloatVector delta, int count ) {
  
  FloatVector r  = start;
  Field[]     fs = new Field[count];
  
  for ( int i = 0; i < count; i++ ) {
    fs[i] = new Field( sys.E( r ), r );
    r     = r.add( delta );
  }
  
  return fs;
}

// keep track of colors of plots and text

public void vColor() { stroke( 204, 102, 0 ); } 

public void vText() { fill( 204, 102, 0 ); }

public void eColor() { stroke( 51, 153, 255 ); }

public void eText() { fill( 51, 153, 255 ); }

  