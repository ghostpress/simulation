public class Particle {

  private final float charge;
  private FloatVector r_p; // position of particle relative to origin

  public Particle( float q, FloatVector location ) {
    charge = q;
    r_p    = location;
  }
  
  public void setPosition(FloatVector position) { r_p = position; }
  
  public FloatVector position() { return r_p; }
  
  public float V( FloatVector r ) {  // r = position of measurement
    
    float distance = sqrt( ( r.x() - r_p.x() ) * ( r.x() - r_p.x() ) + ( r.y() - r_p.y() ) * ( r.y() - r_p.y() ) );
    
    // kq/|r-r_q|
    return ( Constants.K * charge ) / distance;
  }
  
  public FloatVector E( FloatVector r ) {  // r = position of measurement
    
    float distSqd = ( r.x() - r_p.x() ) * ( r.x() - r_p.x() ) + ( r.y() - r_p.y() ) * ( r.y() - r_p.y() );
    float distCbd = distSqd * sqrt( distSqd );
    float common  = -1.0 * Constants.K * charge / distCbd;  // common factor
    return new FloatVector(                                 // electric field vector
                  common * ( r.x() - r_p.x() ),             // x componet of electric field
                  common * ( r.y() - r_p.y() )              // y component of electric field
               );
  }
}