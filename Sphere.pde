public class Sphere {
  
  private final float radius;  // radius of sphere
  private final float charge;  // charge
  private FloatVector r_s;     // position of sphere relative to origin
  
  public Sphere( float r, float q, FloatVector location ) {
    radius = r;
    charge = q;
    r_s    = location;
  }
  
  public void setPosition( FloatVector position ) { r_s = position; }
  
  public FloatVector position() { return r_s; }
  
  public float V( FloatVector r ) {   // r = position of measurement
  
    float distSqd = ( r.x() - r_s.x() ) * ( r.x() - r_s.x() ) + ( r.y() - r_s.y() ) * ( r.y() - r_s.y() );
    float dist    = sqrt( distSqd );
    
    if ( dist < radius ) {  // 3kQ/2R - kQr^2/2R^3
      return ( Constants.K * charge / ( 2.0 * radius ) ) * ( 3.0 - distSqd / ( radius * radius ) );
    }
    else {  // dist >= radius
      return Constants.K * charge / dist;
    }
  }
  
  public FloatVector E( FloatVector r ) {
    
    float distSqd = ( r.x() - r_s.x() ) * ( r.x() - r_s.x() ) + ( r.y() - r_s.y() ) * ( r.y() - r_s.y() );
    float dist    = sqrt( distSqd );
     
    if ( dist < radius ) {
      float common  = -1.0 * Constants.K * charge / ( radius * radius * radius );  // common factor
      return new FloatVector( 
                    common * ( r.x() - r_s.x() ),  // x component of electric field
                    common * ( r.y() - r_s.y() )   // y component of electric field
                 );
    }
    else {  // dist >= radius
      float distCbd = distSqd * dist;
      float common  = -1.0 * Constants.K * charge / distCbd;  // common factor
      return new FloatVector(                                 // electric field vector
                  common * ( r.x() - r_s.x() ),               // x componet of electric field
                  common * ( r.y() - r_s.y() )                // y component of electric field
               );
   }
  }
}