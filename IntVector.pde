public class IntVector {
  
  private int x;
  private int y;
  
  public IntVector( int x, int y ) { 
    this.x = x;
    this.y = y;
  }
  
  public int x() { return x; }
  
  public int y() { return y; }
}