public class FloatVector {
  
  float x; // x-coordinate
  float y; // y-coordinate
  
  public FloatVector( float x, float y ) {
    this.x = x;
    this.y = y;
  }
  
  public float x() { return x; }
  
  public float y() { return y; }
  
  public FloatVector add( FloatVector other) {
    return new FloatVector( x() + other.x(), y() + other.y() );
  }
}