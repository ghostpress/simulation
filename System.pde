public class System {  // models the physical system
  
  private final Particle p;
  private final Sphere s;
  
  public System( Particle p, Sphere s ) {
    this.p = p;
    this.s = s;
  }
  
  public void setParticlePosition( FloatVector position ) { p.setPosition( position ); }
  
  public void setSpherePosition( FloatVector position ) { s.setPosition( position ); }
  
  public Particle particle() { return p; }
  
  public Sphere sphere() { return s; }
  
  public float V( FloatVector r ) {  // the total potential of the system at the given point
    return p.V(r) + s.V(r);
  }  
  
  public FloatVector E( FloatVector r ) { // the total electric field of the system at the given point
    return p.E(r).add( s.E(r) );
  }
}