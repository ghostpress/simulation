public class Field { // the electric field at a specific position
  
  private FloatVector E;
  private FloatVector r;
  
  public Field( FloatVector e, FloatVector r ) {
    this.E = e;
    this.r = r;
  }
  
  public FloatVector E() { return E; }
  
  public FloatVector r() { return r; }
}