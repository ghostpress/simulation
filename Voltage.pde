public class Voltage {  // the potential at a specific position
   
  private float       V;
  private FloatVector r;
  
  public Voltage( float v, FloatVector r ) {
    this.V = v;
    this.r = r;
  }
  
  public float V() { return V; }
  
  public FloatVector r() { return r; }
}